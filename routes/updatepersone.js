var express = require("express");
var router = express.Router();
var fs = require("fs");
var bodyParser = require("body-parser");
var fich = require("../fich");

var urlencodedParser = bodyParser.urlencoded({ extended: false });


router.post("/", urlencodedParser, function(req, res, next) {

    let f= fich;
    let i = 0 ;
    trouveid = parseInt(req.body.id)
    
  //trouver l elemnt dans le fichier pour modifier   
  while (f[i].id !== trouveid ) {
      i=i+1;
    }

    let upv = {};
    upv.id = trouveid
    upv.name = req.body.name
    upv.phone = req.body.phone
    upv.email = req.body.email
     
    f.splice(i, 1, upv);
    let data = JSON.stringify(f);
  
    fs.writeFileSync("fich.json", data, err => {
      if (err) throw err;
    });
 
    let l = fich.length
    res.render("home",{ fich: fich, l: l });
});

module.exports = router;
