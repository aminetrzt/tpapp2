var express = require("express");
var router = express.Router();
var fs = require("fs");
var fich = require("../fich");

var bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/", function(req, res) {
  res.render("addpersone");
});

router.post("/", urlencodedParser, function(req, res) {
  let up = {};
  up.name = req.body.name;
  up.phone = req.body.phone;
  up.email = req.body.email;
  let k;
  let nid;


  if (fich.length>0) {
    k = fich.length - 1;
    nid = fich[k].id + 1;
  } else {
    nid=0;
  }
  
  
  up.id = nid;

  let f = fich;
  f.push(up);
  let data = JSON.stringify(f);

  fs.writeFileSync("fich.json", data, err => {
    if (err) throw err;
  });
  
  let l = fich.length

  res.render("home",{ fich: fich, l: l });
});

module.exports = router;
