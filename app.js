var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fich = require("./fich");


var homeRouter = require('./routes/home');
var addpersoneRouter = require('./routes/addpersone');
var deletepersoneRouter = require('./routes/deletepersone')
var updatepersoneRouter = require('./routes/updatepersone')


var app = express();

app.use('/static', express.static(path.join(__dirname, 'public')))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', homeRouter);
app.use('/addpersone', addpersoneRouter);
app.use('/deletepersone', deletepersoneRouter);
app.use('/updatepersone', updatepersoneRouter);


//recupere le id et les donnees  de l utilisateur pour modifier 

app.get('/updatepersone/:id', function (req, res, next) {
  let f= fich;
  let i = 0 ;
  trouveid = parseInt(req.params.id)
    

while (f[i].id <trouveid ) {i=i+1;}
  element=f[i]
  res.render("updatepersone",{ element : element })
});

////recupere le id et les donnees  de l utilisateur pour supprimer 


app.get('/deletepersone/:id', function (req, res, next) {
  let f= fich;
  let i = 0 ;
  trouveid = parseInt(req.params.id)
    

while (f[i].id <trouveid ) {i=i+1;}
  element=f[i]
  res.render("deletepersone",{ element : element })
});










// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





module.exports = app;

app.listen(3000,() => console.log("Example app listening on port 3000 !! "));
